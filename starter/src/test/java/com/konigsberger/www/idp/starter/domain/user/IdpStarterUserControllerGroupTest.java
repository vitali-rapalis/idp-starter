package com.konigsberger.www.idp.starter.domain.user;

import com.konigsberger.www.idp.starter.config.IdpStarterWebControllerAbstractTest;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DisplayName("User controller group test")
@Disabled
public class IdpStarterUserControllerGroupTest extends IdpStarterWebControllerAbstractTest {

    @Test
    @Disabled
    @DisplayName("Register controller should create user test")
    void registerControllerTest() throws Exception {
        mvc.perform(post("/sign-up?email=example@mail.com&password=123&repeatedPassword=123").with(csrf()))
                .andExpect(status().isOk());
    }
}

