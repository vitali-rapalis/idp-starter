package com.konigsberger.www.idp.starter;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@EnableAutoConfiguration
@SpringBootConfiguration
public class IdpStarterTestConfiguration {

}
