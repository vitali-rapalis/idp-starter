package com.konigsberger.www.idp.starter.config;

import com.konigsberger.www.idp.starter.domain.user.filter.IdpStarterUserSignUpFilter;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.test.web.servlet.MockMvc;

@Disabled
@IdpStarterWebControllerAnnotation
public class IdpStarterWebControllerAbstractTest {

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected SecurityFilterChain securityFilterChain;

    @Autowired
    protected IdpStarterUserSignUpFilter signUpFilter;

    @Test
    @Disabled
    @DisplayName("Dependencies for this test should not be null")
    void contextTest() {
        Assertions.assertThat(mvc).isNotNull();
//        Assertions.assertThat(securityFilterChain.getFilters()).containsAnyOf(signUpFilter);
    }
}