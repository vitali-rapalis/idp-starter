package com.konigsberger.www.idp.starter.domain.common.controller;

import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterSignUpException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class IdpStarterCommonControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IdpStarterSignUpException.class)
    public ResponseEntity<Object> handleEntityExistsException(IdpStarterSignUpException ex, WebRequest request) {
        return handleExceptionInternal(ex, "", new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
