package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.user.repository.IdpStarterUserSignUpCodeRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class IdpStarterUserSignUpCodeServiceImpl implements IdpStarterUserSignUpCodeService {

    private final IdpStarterUserSignUpCodeRepository userSignUpCodeRepository;

    @Override
    @Transactional(readOnly = true)
    public UUID getUserId(@NonNull UUID code) {
        return userSignUpCodeRepository.findByCode(code).orElseThrow(EntityNotFoundException::new).getUserId();
    }

    @Override
    public void deleteByCode(UUID code) {
        userSignUpCodeRepository.deleteByCode(code);
    }

}
