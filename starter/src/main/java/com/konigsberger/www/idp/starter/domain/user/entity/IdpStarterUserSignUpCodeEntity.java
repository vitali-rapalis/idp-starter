package com.konigsberger.www.idp.starter.domain.user.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "idp_user_signup_code")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterUserSignUpCodeEntity implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false, unique = true)
    @EqualsAndHashCode.Include
    private UUID userId;

    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, unique = true)
    @Builder.Default
    private UUID code = UUID.randomUUID();

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private IdpStarterUserEntity userEntity;
}
