package com.konigsberger.www.idp.starter.domain.user.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "idp_user_account")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterUserAccountEntity implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false, unique = true)
    @Setter(AccessLevel.NONE)
    @EqualsAndHashCode.Include
    private UUID userId;

    @NotNull
    @Column(nullable = false)
    @Builder.Default
    private Boolean emailConfirmed = false;

    @NotNull
    @Column(nullable = false)
    @Builder.Default
    private Boolean accountNonExpired = false;

    @NotNull
    @Column(nullable = false)
    @Builder.Default
    private Boolean accountNonLocked = false;

    @NotNull
    @Column(nullable = false)
    @Builder.Default
    private Boolean credentialsNonExpired = false;

    @NotNull
    @Column(nullable = false)
    @Builder.Default
    private Boolean isEnabled = false;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    @JsonBackReference
    private IdpStarterUserEntity userEntity;
}
