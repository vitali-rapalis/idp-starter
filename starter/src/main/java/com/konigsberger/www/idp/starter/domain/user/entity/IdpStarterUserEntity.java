package com.konigsberger.www.idp.starter.domain.user.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.konigsberger.www.idp.starter.domain.role.entity.IdpStarterRoleEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import net.minidev.json.annotate.JsonIgnore;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;
import org.hibernate.annotations.UuidGenerator;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "idp_user")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterUserEntity implements Serializable {

    @Id
    @NotNull
    @GeneratedValue
    @UuidGenerator
    @Column(unique = true, nullable = false)
    @Setter(AccessLevel.NONE)
    @EqualsAndHashCode.Include
    private UUID id;

    @NotNull
    @NaturalId
    @Column(unique = true, nullable = false, length = 254)
    private String email;

    @NotNull
    @Column(nullable = false, length = 60)
    private String password;

    @OneToOne(cascade = CascadeType.ALL, optional = false, mappedBy = "userEntity", fetch = FetchType.EAGER)
    @ToString.Exclude
    @JsonManagedReference
    private IdpStarterUserAccountEntity userAccountEntity;

    @OneToOne(cascade = CascadeType.ALL, optional = false, mappedBy = "userEntity", fetch = FetchType.EAGER)
    @ToString.Exclude
    @JsonManagedReference
    private IdpStarterUserInfoEntity userInfoEntity;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userEntity")
    @ToString.Exclude
    private IdpStarterUserSignUpCodeEntity signUpCodeEntity;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userEntity")
    @ToString.Exclude
    private IdpStarterUserRestorePasswordCodeEntity restorePasswordCodeEntity;

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "idp_user_has_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_name", referencedColumnName = "name")
    )
    @Builder.Default
    @ToString.Exclude
    private Set<IdpStarterRoleEntity> roleEntities = new HashSet<>();

    public void addRole(@NonNull IdpStarterRoleEntity roleEntity) {
        roleEntities.add(roleEntity);
        roleEntity.getUserEntities().add(this);
    }

    public void removeRole(@NonNull IdpStarterRoleEntity roleEntity) {
        roleEntities.remove(roleEntity);
        roleEntity.getUserEntities().remove(this);
    }

    public Set<IdpStarterRoleEntity> getRoleEntities() {
        return Collections.unmodifiableSet(roleEntities);
    }
}
