package com.konigsberger.www.idp.starter.domain.user.controller;

import com.konigsberger.www.idp.starter.domain.common.controller.IdpStarterCommonControllerModel.Modal;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonControllerModelUtility;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonResponseMsgUtility;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserResponseMsgUtility;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserUrlUtility;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Controller
public class IdpStarterUserController {

    @GetMapping("/home")
    public String home() {
        return "index";
    }

    @GetMapping(IdpStarterUserUrlUtility.SIGN_IN)
    public String signIn(@RequestParam(required = false) Optional<String> error,
                         @RequestParam(required = false) Optional<String> success, Model model) {
        IdpStarterCommonControllerModelUtility.createCommonModelForSignIn(model, error, success);
        return "sign-in";
    }

    @GetMapping(IdpStarterUserUrlUtility.SIGN_UP)
    public String signUp(@RequestParam(required = false) Optional<String> error, Model model) {
        IdpStarterCommonControllerModelUtility.createCommonModel(error, model, Modal.EModalType.ERROR,
                IdpStarterCommonResponseMsgUtility.ERROR_TITLE, IdpStarterUserResponseMsgUtility.SIGN_UP_ERROR_TEXT);
        return "sign-up";
    }

    @GetMapping(IdpStarterUserUrlUtility.SIGN_UP_SUCCESS)
    public String signUpSuccess(Model model) {
        model.addAttribute("title", IdpStarterUserResponseMsgUtility.SIGN_UP_SUCCESS_TITLE);
        model.addAttribute("text", IdpStarterUserResponseMsgUtility.SIGN_UP_SUCCESS_TEXT);
        return "success";
    }

    @GetMapping(IdpStarterUserUrlUtility.SIGN_OUT)
    public String signOut(Model model) {
        return "sign-out";
    }

    @GetMapping(IdpStarterUserUrlUtility.RESTORE_PASSWORD)
    public String restorePassword(@RequestParam(required = false) Optional<String> error, Model model) {
        IdpStarterCommonControllerModelUtility.createCommonModel(error, model, Modal.EModalType.ERROR,
                IdpStarterCommonResponseMsgUtility.ERROR_TITLE, IdpStarterUserResponseMsgUtility.RESTORE_PASSWORD_ERROR_TEXT);
        return "restore-password";
    }

    @GetMapping(IdpStarterUserUrlUtility.RESTORE_PASSWORD_SUCCESS)
    public String restorePasswordSuccess(Model model) {
        model.addAttribute("title", IdpStarterUserResponseMsgUtility.RESTORE_PASSWORD_SUCCESS_TITLE);
        model.addAttribute("text", IdpStarterUserResponseMsgUtility.RESTORE_PASSWORD_SUCCESS_TEXT);
        return "success";
    }

    @GetMapping(IdpStarterUserUrlUtility.CHANGE_PASSWORD)
    public String changePassword(@RequestParam(required = false) Optional<String> error, Model model) {
        IdpStarterCommonControllerModelUtility.createCommonModel(error, model, Modal.EModalType.ERROR,
                IdpStarterCommonResponseMsgUtility.ERROR_TITLE, IdpStarterUserResponseMsgUtility.CHANGE_PASSWORD_ERROR_TEXT);
        return "change-password";
    }
}
