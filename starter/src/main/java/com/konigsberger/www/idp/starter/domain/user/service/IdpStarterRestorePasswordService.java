package com.konigsberger.www.idp.starter.domain.user.service;


import com.konigsberger.www.idp.starter.domain.email.error.IdpStarterSendEmailError;

public interface IdpStarterRestorePasswordService {

    void restorePassword(String email) throws IdpStarterSendEmailError;
}
