package com.konigsberger.www.idp.starter.domain.common.controller;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterCommonControllerModel {
    private Modal modal;

    @Data
    @Builder
    @AllArgsConstructor
    public static class Modal {
        private EModalType type;
        private String title;
        private String text;

        @Getter
        @AllArgsConstructor
        public enum EModalType {
            PRIMARY("alert-primary"), INFO("alert-info"), SUCCESS("alert-success"),
            WARNING("alert-warning"), ERROR("alert-danger");
            private String cssClass;
        }
    }
}
