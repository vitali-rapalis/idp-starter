package com.konigsberger.www.idp.starter.domain.user.repository;

import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IdpStarterUserAccountRepository extends JpaRepository<IdpStarterUserAccountEntity, UUID> {
}
