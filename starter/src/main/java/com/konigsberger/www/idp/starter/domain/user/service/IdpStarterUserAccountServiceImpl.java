package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterSignUpConfirmException;
import com.konigsberger.www.idp.starter.domain.user.repository.IdpStarterUserAccountRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class IdpStarterUserAccountServiceImpl implements IdpStarterUserAccountService {

    private final IdpStarterUserAccountRepository userAccountRepository;

    private final IdpStarterUserSignUpCodeService userSignUpCodeService;

    @Override
    public void signUpConfirm(@NonNull UUID code) throws IdpStarterSignUpConfirmException {
        try {
            val userId = userSignUpCodeService.getUserId(code);
            val userAccountEntity = userAccountRepository.findById(userId).orElseThrow(EntityNotFoundException::new);
            userAccountEntity.setEmailConfirmed(true);
            userAccountEntity.setAccountNonLocked(true);
            userAccountEntity.setAccountNonExpired(true);
            userAccountEntity.setCredentialsNonExpired(true);
            userAccountEntity.setIsEnabled(true);

            userSignUpCodeService.deleteByCode(code);
        } catch (Exception ex) {
            throw new IdpStarterSignUpConfirmException(ex);
        }
    }
}
