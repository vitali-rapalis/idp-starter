package com.konigsberger.www.idp.starter.domain.user.utility;

import lombok.experimental.UtilityClass;

@UtilityClass
public class IdpStarterUserUrlUtility {

    public static final String SIGN_IN = "/sign-in";
    public static final String SIGN_UP_CONFIRM = "/sign-up-confirm";
    public static final String SIGN_UP = "/sign-up";
    public static final String SIGN_UP_SUCCESS = "/sign-up-success";
    public static final String SIGN_OUT = "/sign-out";
    public static final String RESTORE_PASSWORD = "/restore-password";
    public static final String CHANGE_PASSWORD = "/change-password";
    public static final String RESTORE_PASSWORD_SUCCESS = RESTORE_PASSWORD + "-success";
    private static final String ERROR_PARAMETER = "?error=";
    public static final String SIGN_IN_ERROR = SIGN_IN + ERROR_PARAMETER;
    private static final String SUCCESS_PARAMETER = "?success=";
    public static final String SIGN_IN_SUCCESS = SIGN_IN + SUCCESS_PARAMETER;
    }
