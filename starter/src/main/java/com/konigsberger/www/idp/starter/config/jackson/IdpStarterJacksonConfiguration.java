package com.konigsberger.www.idp.starter.config.jackson;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserAccountEntity;
import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserEntity;
import com.konigsberger.www.idp.starter.domain.user.mixin.IdpStarterUserEntitiesJsonMixins;
import com.konigsberger.www.idp.starter.domain.user.model.IdpStarterUserDetailsModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.jackson2.CoreJackson2Module;
import org.springframework.security.jackson2.SecurityJackson2Modules;
import org.springframework.security.oauth2.client.jackson2.OAuth2ClientJackson2Module;
import org.springframework.security.oauth2.server.authorization.JdbcOAuth2AuthorizationService;
import org.springframework.security.oauth2.server.authorization.jackson2.OAuth2AuthorizationServerJackson2Module;
import org.springframework.security.web.jackson2.WebJackson2Module;
import org.springframework.security.web.jackson2.WebServletJackson2Module;

@Configuration
public class IdpStarterJacksonConfiguration {

    @Bean("idpStarterObjectMapper")
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        ClassLoader classLoader = JdbcOAuth2AuthorizationService.class.getClassLoader();
        objectMapper.registerModules(new CoreJackson2Module());
        objectMapper.registerModules(SecurityJackson2Modules.getModules(classLoader));
        objectMapper.registerModules(new OAuth2ClientJackson2Module());
        objectMapper.registerModule(new OAuth2AuthorizationServerJackson2Module());
        objectMapper.registerModule(new WebJackson2Module());
        objectMapper.registerModule(new WebServletJackson2Module());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

        objectMapper.addMixIn(IdpStarterUserEntity.class, IdpStarterUserEntitiesJsonMixins.UserEntityMixin.class);
        objectMapper.addMixIn(IdpStarterUserAccountEntity.class, IdpStarterUserEntitiesJsonMixins.UserAccountEntityMixin.class);
        objectMapper.addMixIn(IdpStarterUserDetailsModel.class, IdpStarterUserEntitiesJsonMixins.UserDetailsMixin.class);

        return objectMapper;
    }
}
