package com.konigsberger.www.idp.starter.domain.user.repository;

import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserRestorePasswordCodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IdpStarterRestorePasswordCodeEntityRepository extends JpaRepository<IdpStarterUserRestorePasswordCodeEntity, UUID> {

    Optional<IdpStarterUserRestorePasswordCodeEntity> findByCode(UUID code);
}
