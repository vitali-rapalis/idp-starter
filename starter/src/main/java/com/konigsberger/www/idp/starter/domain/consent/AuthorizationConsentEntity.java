package com.konigsberger.www.idp.starter.domain.consent;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "authorization_consent")
@IdClass(AuthorizationConsentEntity.AuthorizationConsentId.class)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizationConsentEntity implements Serializable{

    @Id
	private String registeredClientId;

    @Id
	private String principalName;

    @Column(length = 1000)
	private String authorities;

    public static class AuthorizationConsentId implements Serializable {
		private String registeredClientId;
		private String principalName;

		// @fold:on
		public String getRegisteredClientId() {
			return registeredClientId;
		}

		public void setRegisteredClientId(String registeredClientId) {
			this.registeredClientId = registeredClientId;
		}

		public String getPrincipalName() {
			return principalName;
		}

		public void setPrincipalName(String principalName) {
			this.principalName = principalName;
		}
		// @fold:off

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			AuthorizationConsentId that = (AuthorizationConsentId) o;
			return registeredClientId.equals(that.registeredClientId) && principalName.equals(that.principalName);
		}

		@Override
		public int hashCode() {
			return Objects.hash(registeredClientId, principalName);
		}
	}
}
