package com.konigsberger.www.idp.starter.domain.user.model;

import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterUserDetailsModel implements UserDetails {

    private IdpStarterUserEntity userEntity;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return userEntity.getRoleEntities().stream()
                .flatMap(roleEntity -> roleEntity.getAuthorityEntities().stream())
                .map(authorityEntity -> new SimpleGrantedAuthority(authorityEntity.getName()))
                .toList();
    }

    @Override
    public String getPassword() {
        return userEntity.getPassword();
    }

    @Override
    public String getUsername() {
        return userEntity.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return userEntity.getUserAccountEntity().getAccountNonExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return userEntity.getUserAccountEntity().getAccountNonLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return userEntity.getUserAccountEntity().getCredentialsNonExpired();
    }

    @Override
    public boolean isEnabled() {
        return userEntity.getUserAccountEntity().getIsEnabled();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (IdpStarterUserDetailsModel) obj;
        return Objects.equals(this.userEntity, that.userEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userEntity);
    }

    @Override
    public String toString() {
        return "IdpStarterUserDetailsModel[" +
                "userEntity=" + userEntity + ']';
    }

}
