package com.konigsberger.www.idp.starter.domain.email.service;

import com.konigsberger.www.idp.starter.domain.email.dto.IdpStarterEmailSendDto;
import com.konigsberger.www.idp.starter.domain.email.error.IdpStarterSendEmailError;
import com.konigsberger.www.idp.starter.domain.email.property.IdpStarterSpringEmailProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@RequiredArgsConstructor
public class IdpStarterEmailServiceImpl implements IdpStarterEmailService {

    private final JavaMailSender emailSender;
    private final IdpStarterSpringEmailProperties emailProperties;

    @Override
    public void sendSimpleMessage(String to, String subject, String text) throws IdpStarterSendEmailError {
        try {
            val message = createSimpleMailMessage(to, subject, text);
            emailSender.send(message);
        } catch (Exception ex) {
            sendEmailHandleException(ex);
        }
    }

    @Override
    public void sendMimeMessage(IdpStarterEmailSendDto email) throws IdpStarterSendEmailError {
        try {
            val mimeMessage = emailSender.createMimeMessage();
            val helper = new MimeMessageHelper(mimeMessage, "utf-8");
            val htmlMsg = email.getText();
            helper.setText(htmlMsg, true);
            helper.setTo(email.getMailTo());
            helper.setSubject(email.getSubject());
            helper.setFrom(emailProperties.getUsername());
            emailSender.send(mimeMessage);
        } catch (Exception ex) {
            sendEmailHandleException(ex);
        }
    }

    private SimpleMailMessage createSimpleMailMessage(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(emailProperties.getUsername());
        message.setSubject(subject);
        message.setText(text);
        return message;
    }

    private static void sendEmailHandleException(Exception ex) throws IdpStarterSendEmailError {
        log.error("Send email failed, reason: {}", ex.getMessage());
        throw new IdpStarterSendEmailError(ex);
    }
}
