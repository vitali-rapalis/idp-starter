package com.konigsberger.www.idp.starter.domain.user.repository;

import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserSignUpCodeEntity;
import com.konigsberger.www.idp.starter.domain.user.projection.IdpStarterOnlyUserIdProjection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IdpStarterUserSignUpCodeRepository extends CrudRepository<IdpStarterUserSignUpCodeEntity, UUID> {

    Optional<IdpStarterOnlyUserIdProjection> findByCode(UUID code);

    void deleteByCode(UUID code);
}
