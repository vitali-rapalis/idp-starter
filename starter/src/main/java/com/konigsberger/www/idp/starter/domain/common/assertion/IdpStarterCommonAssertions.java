package com.konigsberger.www.idp.starter.domain.common.assertion;

import lombok.experimental.UtilityClass;
import lombok.val;
import org.springframework.security.core.context.SecurityContextHolder;

@UtilityClass
public class IdpStarterCommonAssertions {

    private static final String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";

    public static void assertNotAuthenticated() {
        val authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null) {
            if (authentication.isAuthenticated()) {
                authentication.getAuthorities().forEach(grantedAuthority -> {
                    if (!grantedAuthority.getAuthority().equalsIgnoreCase(ROLE_ANONYMOUS)) {
                        throw new IllegalStateException();
                    }
                });
            }
        }
    }

}
