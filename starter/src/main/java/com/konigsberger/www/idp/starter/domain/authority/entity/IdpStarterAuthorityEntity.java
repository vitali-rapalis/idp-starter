package com.konigsberger.www.idp.starter.domain.authority.entity;

import com.konigsberger.www.idp.starter.domain.role.entity.IdpStarterRoleEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "idp_authority")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterAuthorityEntity implements Serializable {

    @Id
    @Column(length = 30, nullable = false)
    @EqualsAndHashCode.Include
    private String name;

    @Column(length = 160, nullable = false)
    private String description;

    @ManyToMany(mappedBy = "authorityEntities")
    @Builder.Default
    @ToString.Exclude
    private Set<IdpStarterRoleEntity> roleEntities = new HashSet<>();
}
