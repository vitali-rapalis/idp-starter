package com.konigsberger.www.idp.starter.domain.email.error;

public class IdpStarterSendEmailError extends Exception {
    private final static String MSG = "Send email failed.";

    public IdpStarterSendEmailError(Throwable cause) {
        super(MSG, cause);
    }
}
