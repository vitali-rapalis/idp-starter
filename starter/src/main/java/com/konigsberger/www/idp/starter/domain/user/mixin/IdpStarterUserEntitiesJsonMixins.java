package com.konigsberger.www.idp.starter.domain.user.mixin;

import com.fasterxml.jackson.annotation.*;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.UUID;

public class IdpStarterUserEntitiesJsonMixins {

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE,
            isGetterVisibility = JsonAutoDetect.Visibility.NONE)
    @JsonIgnoreProperties(value = {"authorities", "enabled"}, ignoreUnknown = true, allowGetters = true)
    public static class UserDetailsMixin {

        @JsonCreator
        public UserDetailsMixin(@JsonProperty("authorities") Collection<? extends GrantedAuthority> authorities,
                                @JsonProperty("password") String password, @JsonProperty("username") String username,
                                @JsonProperty("accountNonLocked") Boolean accountNonLocked,
                                @JsonProperty("accountNonExpired") Boolean accountNonExpired,
                                @JsonProperty("credentialsNonExpired") Boolean credentialsNonExpired,
                                @JsonProperty("enabled") Boolean enabled) {
        }
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE,
            isGetterVisibility = JsonAutoDetect.Visibility.NONE)
    @JsonIgnoreProperties({"userInfoEntity", "signUpCodeEntity", "restorePasswordCodeEntity", "roleEntities"})
    public static class UserEntityMixin {

        @JsonCreator
        public UserEntityMixin(@JsonProperty("id") UUID id, @JsonProperty("email") String email,
                               @JsonProperty("password") String password) {
        }
    }

    @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
    @JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE,
            isGetterVisibility = JsonAutoDetect.Visibility.NONE)
    @JsonIgnoreProperties({"userEntity"})
    public static class UserAccountEntityMixin {

        @JsonCreator
        public UserAccountEntityMixin(@JsonProperty("userId") UUID userId,
                                      @JsonProperty("emailConfirmed") Boolean emailConfirmed,
                                      @JsonProperty("accountNonExpired") Boolean accountNonExpired,
                                      @JsonProperty("accountNonLocked") Boolean accountNonLocked,
                                      @JsonProperty("credentialsNonExpired") Boolean credentialsNonExpired,
                                      @JsonProperty("isEnabled") Boolean isEnabled) {
        }
    }

}
