package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterSignUpConfirmException;

import java.util.UUID;

public interface IdpStarterUserAccountService {

    void signUpConfirm(UUID userId) throws IdpStarterSignUpConfirmException;
}
