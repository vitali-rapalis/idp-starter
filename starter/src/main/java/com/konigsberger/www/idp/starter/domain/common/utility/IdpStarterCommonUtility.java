package com.konigsberger.www.idp.starter.domain.common.utility;

import lombok.experimental.UtilityClass;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;

@UtilityClass
public class IdpStarterCommonUtility {
    static void findResources() {
        final var mainCssResource = new ClassPathResource("public/main.css",
                IdpStarterCommonUtility.class.getClass().getClassLoader());

        System.out.println(String.format("""
                File exists: %s
                File name: %s
                File path: %s
                """, mainCssResource.exists(), mainCssResource.getFilename(), mainCssResource.getPath()));

        final File file;
        try {
            file = mainCssResource.getFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Absolute path:" + file.getAbsolutePath());
    }

}
