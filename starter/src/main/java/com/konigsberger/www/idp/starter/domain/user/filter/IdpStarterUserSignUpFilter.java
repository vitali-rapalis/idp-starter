package com.konigsberger.www.idp.starter.domain.user.filter;

import com.konigsberger.www.idp.starter.domain.common.assertion.IdpStarterCommonAssertions;
import com.konigsberger.www.idp.starter.domain.user.service.IdpStarterUserService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import lombok.val;

import java.io.IOException;

@Log4j2
@Component
@RequiredArgsConstructor
public class IdpStarterUserSignUpFilter extends OncePerRequestFilter {

    private static final String SIGN_UP_FILTER_SERVLET_PATH = "/sign-up";
    private final IdpStarterUserService userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        try {

            IdpStarterCommonAssertions.assertNotAuthenticated();

            val email = request.getParameter("email");
            val password = request.getParameter("password");
            val passwordRepeated = request.getParameter("repeatedPassword");

            if (email == null || email.isBlank() || password == null || password.isBlank() || passwordRepeated == null
                    || passwordRepeated.isBlank()) {
                // TODO
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "All fields are required.");
                return;
            }

            if (!password.equals(passwordRepeated)) {
                response.sendRedirect(SIGN_UP_FILTER_SERVLET_PATH + "?error");
                return;
            }

            userService.signUp(email, password);
            response.sendRedirect(SIGN_UP_FILTER_SERVLET_PATH + "-success");
            filterChain.doFilter(request, response);
        } catch (Exception ex) {
            log.error("Sign up failed, reason: {}", ex.getMessage());
            response.sendRedirect(SIGN_UP_FILTER_SERVLET_PATH + "?error");
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        var signUpUrlAndPostMethod = request.getRequestURI().equals(SIGN_UP_FILTER_SERVLET_PATH) && request.getMethod().equalsIgnoreCase(HttpMethod.POST.name());
        return !signUpUrlAndPostMethod;
    }
}
