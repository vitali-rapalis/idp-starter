package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.user.model.IdpStarterUserDetailsModel;
import com.konigsberger.www.idp.starter.domain.user.repository.IdpStarterUserEntityRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class IdpStarterUserDetailsServiceImp implements UserDetailsService {

    private final IdpStarterUserEntityRepository userEntityRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        val foundUserEntity = userEntityRepository.findByEmail(email)
                .orElseThrow(EntityNotFoundException::new);
        return new IdpStarterUserDetailsModel(foundUserEntity);
    }
}
