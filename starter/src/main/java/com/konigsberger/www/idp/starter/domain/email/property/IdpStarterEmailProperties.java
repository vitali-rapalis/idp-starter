package com.konigsberger.www.idp.starter.domain.email.property;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Builder
@Validated
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("idp.mail")
public class IdpStarterEmailProperties {

    public final static String LINK_REGEX = "&\\{link\\}";

    @NotNull
    private SignUpEmail signUp;

    @NotNull
    private RestorePasswordEmail restorePassword;

    @NotBlank
    private String idpServerAddress;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SignUpEmail {

        @NotBlank
        private String subject;

        @NotBlank
        private String text;

    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RestorePasswordEmail {

        @NotBlank
        private String subject;

        @NotBlank
        private String text;
    }
}
