package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterChangePasswordException;
import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterSignUpException;
import com.konigsberger.www.idp.starter.domain.user.model.IdpStarterUserDetailsModel;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public interface IdpStarterUserService {

    void signUp(String email, String password) throws IdpStarterSignUpException;

    IdpStarterUserDetailsModel changePassword(@NotNull UUID code, @NotNull String newPassword) throws IdpStarterChangePasswordException;
}
