package com.konigsberger.www.idp.starter.domain.user.filter;

import com.konigsberger.www.idp.starter.domain.common.assertion.IdpStarterCommonAssertions;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonBase64Utility;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonControllerModelUtility;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonUtility;
import com.konigsberger.www.idp.starter.domain.email.property.IdpStarterEmailProperties;
import com.konigsberger.www.idp.starter.domain.user.service.IdpStarterUserService;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserResponseMsgUtility;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserUrlUtility;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.UUID;

@Log4j2
@Component
@RequiredArgsConstructor
public class IdpStarterChangePasswordFilter extends OncePerRequestFilter {

    private final IdpStarterUserService userService;

    private final RestTemplate restTemplate;

    private final IdpStarterEmailProperties emailProperties;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        try {

            IdpStarterCommonAssertions.assertNotAuthenticated();

            val code = request.getParameter("code");
            val password = request.getParameter("password");
            val repeatedPassword = request.getParameter("repeatedPassword");

            if (code == null && password == null || repeatedPassword == null) {
                IdpStarterCommonControllerModelUtility
                        .redirectToError(response, IdpStarterUserUrlUtility.CHANGE_PASSWORD,
                                IdpStarterUserResponseMsgUtility.CHANGE_PASSWORD_BAD_REQUEST);
                return;
            }

            if (code.isBlank() || password.isBlank() || repeatedPassword.isBlank()) {
                IdpStarterCommonControllerModelUtility
                        .redirectToError(response, IdpStarterUserUrlUtility.CHANGE_PASSWORD,
                                IdpStarterUserResponseMsgUtility.CHANGE_PASSWORD_BAD_REQUEST);
                return;
            }

            if (!password.equals(repeatedPassword)) {
                IdpStarterCommonControllerModelUtility
                        .redirectToError(response, IdpStarterUserUrlUtility.CHANGE_PASSWORD,
                                IdpStarterUserResponseMsgUtility.CHANGE_PASSWORD_BAD_REQUEST);
                return;
            }

            val user = userService.changePassword(UUID.fromString(code), password);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

            MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
            map.add("email", user.getUsername());
            map.add("password", password);

            HttpEntity<MultiValueMap<String, String>> req = new HttpEntity<MultiValueMap<String, String>>(map, headers);

            ResponseEntity<?> res = restTemplate.postForEntity(emailProperties.getIdpServerAddress() +
                    IdpStarterUserUrlUtility.SIGN_IN, req, String.class);

            res.getHeaders().forEach((header, headerValues) -> {
                headerValues.forEach(headerValue -> {
                    response.setHeader(header, headerValue);
                });
            });

            response.sendRedirect("/");
        } catch (Exception ex) {
            log.error("Change password failed, reason: {}", ex.getMessage());
            IdpStarterCommonControllerModelUtility
                    .redirectToError(response, IdpStarterUserUrlUtility.CHANGE_PASSWORD,
                            IdpStarterUserResponseMsgUtility.CHANGE_PASSWORD_ERROR_TEXT);
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        var changePasswordPutMethod = request.getRequestURI().equals(IdpStarterUserUrlUtility.CHANGE_PASSWORD) && request.getMethod().equalsIgnoreCase(HttpMethod.POST.name());
        return !changePasswordPutMethod;
    }
}
