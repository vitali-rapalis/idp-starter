package com.konigsberger.www.idp.starter.domain.user.projection;

import java.util.UUID;

public interface IdpStarterOnlyUserIdProjection {

    UUID getUserId();
}
