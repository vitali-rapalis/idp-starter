package com.konigsberger.www.idp.starter.domain.common.utility;

import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@UtilityClass
public class IdpStarterCommonBase64Utility {

    public String encodeMessage(String msg) {
        byte[] messageBytes = msg.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(messageBytes);
    }

    public String decodeMessage(String encodedMsg) {
        byte[] decodedMessageBytes = Base64.getDecoder().decode(encodedMsg);
        return new String(decodedMessageBytes, StandardCharsets.UTF_8);
    }
}
