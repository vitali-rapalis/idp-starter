package com.konigsberger.www.idp.starter.domain.email.property;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@Data
@Builder
@Validated
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("spring.mail")
public class IdpStarterSpringEmailProperties {

    @NotNull
    private String host;

    @NotNull
    private int port;

    @Email
    @NotNull
    private String username;
}
