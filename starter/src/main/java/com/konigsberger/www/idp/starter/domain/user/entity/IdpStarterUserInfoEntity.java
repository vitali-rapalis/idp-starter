package com.konigsberger.www.idp.starter.domain.user.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "idp_user_info")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterUserInfoEntity implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false, unique = true)
    @EqualsAndHashCode.Include
    private UUID userId;

    private Short age;

    @Column(length = 24)
    private String firstName;

    @Column(length = 24)
    private String lastName;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    @JsonBackReference
    private IdpStarterUserEntity userEntity;
}
