package com.konigsberger.www.idp.starter.domain.role.entity;

import com.konigsberger.www.idp.starter.domain.authority.entity.IdpStarterAuthorityEntity;
import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "idp_role")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterRoleEntity implements Serializable {

    @Id
    @Column(length = 30, nullable = false)
    @EqualsAndHashCode.Include
    private String name;

    @Column(length = 160, nullable = false)
    private String description;

    @ManyToMany(mappedBy = "roleEntities")
    @Builder.Default
    private Set<IdpStarterUserEntity> userEntities = new HashSet<>();

    @ManyToMany(
            fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(
            name = "idp_role_has_authority",
            joinColumns = @JoinColumn(name = "role_name", referencedColumnName = "name"),
            inverseJoinColumns = @JoinColumn(name = "authority_name", referencedColumnName = "name")
    )
    @Builder.Default
    @ToString.Exclude
    private Set<IdpStarterAuthorityEntity> authorityEntities = new HashSet<>();
}
