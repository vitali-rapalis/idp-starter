package com.konigsberger.www.idp.starter.domain.user.filter;

import com.konigsberger.www.idp.starter.domain.common.assertion.IdpStarterCommonAssertions;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonBase64Utility;
import com.konigsberger.www.idp.starter.domain.user.service.IdpStarterUserAccountService;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserResponseMsgUtility;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserUrlUtility;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.UUID;

@Log4j2
@Component
@RequiredArgsConstructor
public class IdpStarterUserSignUpConfirmFilter extends OncePerRequestFilter {

    private final IdpStarterUserAccountService userAccountService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        try {
            IdpStarterCommonAssertions.assertNotAuthenticated();
            val code = request.getParameter("code");

            if (code == null) {
                // TODO
            }

            if (code.isBlank()) {
                // TODO
            }

            UUID codeAsUUID = UUID.fromString(code);
            userAccountService.signUpConfirm(codeAsUUID);

            String successMsg = IdpStarterCommonBase64Utility.encodeMessage(IdpStarterUserResponseMsgUtility.SIGN_UP_CONFIRM_SUCCESS_MSG);
            response.sendRedirect(IdpStarterUserUrlUtility.SIGN_IN_SUCCESS + successMsg);
        } catch (Exception ex) {
            log.error("Sign up confirm failed, reason: {}", ex.getMessage());
            val errorMsg = IdpStarterCommonBase64Utility.encodeMessage(IdpStarterUserResponseMsgUtility.SIGN_UP_CONFIRM_FAILED_MSG);
            response.sendRedirect(IdpStarterUserUrlUtility.SIGN_IN_ERROR + errorMsg);
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        var signUpUrlAndPostMethod = request.getRequestURI().equals(IdpStarterUserUrlUtility.SIGN_UP_CONFIRM)
                && request.getMethod().equalsIgnoreCase(HttpMethod.GET.name());
        return !signUpUrlAndPostMethod;
    }
}
