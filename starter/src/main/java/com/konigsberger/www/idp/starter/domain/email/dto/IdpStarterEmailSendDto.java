package com.konigsberger.www.idp.starter.domain.email.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterEmailSendDto {

    @Email
    @NotNull
    private String mailTo;

    @NotNull
    @Size(min = 1, max = 160)
    private String subject;

    @NotNull
    @Size(min = 1, max = 560)
    private String text;
}
