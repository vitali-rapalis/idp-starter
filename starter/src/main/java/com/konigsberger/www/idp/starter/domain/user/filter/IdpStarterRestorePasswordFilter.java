package com.konigsberger.www.idp.starter.domain.user.filter;

import com.konigsberger.www.idp.starter.domain.common.assertion.IdpStarterCommonAssertions;
import com.konigsberger.www.idp.starter.domain.common.utility.IdpStarterCommonBase64Utility;
import com.konigsberger.www.idp.starter.domain.user.service.IdpStarterRestorePasswordService;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserResponseMsgUtility;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserUrlUtility;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Log4j2
@Component
@RequiredArgsConstructor
public class IdpStarterRestorePasswordFilter extends OncePerRequestFilter {

    private static final String RESTORE_PASSWORD_FILTER_SERVLET_PATH = "/restore-password";

    private final IdpStarterRestorePasswordService restorePasswordService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException {
        try {

            IdpStarterCommonAssertions.assertNotAuthenticated();

            val email = request.getParameter("email");

            if (email == null) {
                response.sendRedirect(RESTORE_PASSWORD_FILTER_SERVLET_PATH + "?error");
                return;
            }

            if (email.isBlank()) {
                response.sendRedirect(RESTORE_PASSWORD_FILTER_SERVLET_PATH + "?error");
                return;
            }

            restorePasswordService.restorePassword(email);
            response.sendRedirect(IdpStarterUserUrlUtility.RESTORE_PASSWORD_SUCCESS);
        } catch (Exception ex) {
            log.error("Restore password failed, reason: {}", ex.getMessage());
            val errorMsg = IdpStarterCommonBase64Utility.encodeMessage(IdpStarterUserResponseMsgUtility.RESTORE_PASSWORD_ERROR_TEXT);
            response.sendRedirect(RESTORE_PASSWORD_FILTER_SERVLET_PATH + "?error=" + errorMsg);
        }
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) {
        var signUpUrlAndPostMethod = request.getRequestURI().equals(RESTORE_PASSWORD_FILTER_SERVLET_PATH) && request.getMethod().equalsIgnoreCase(HttpMethod.POST.name());
        return !signUpUrlAndPostMethod;
    }
}
