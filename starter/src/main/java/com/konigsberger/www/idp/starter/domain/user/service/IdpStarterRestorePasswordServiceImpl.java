package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.email.dto.IdpStarterEmailSendDto;
import com.konigsberger.www.idp.starter.domain.email.error.IdpStarterSendEmailError;
import com.konigsberger.www.idp.starter.domain.email.property.IdpStarterEmailProperties;
import com.konigsberger.www.idp.starter.domain.email.service.IdpStarterEmailService;
import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserRestorePasswordCodeEntity;
import com.konigsberger.www.idp.starter.domain.user.repository.IdpStarterUserEntityRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Log4j2
@Service
@Transactional
@RequiredArgsConstructor
public class IdpStarterRestorePasswordServiceImpl implements IdpStarterRestorePasswordService {

    private final IdpStarterUserEntityRepository userEntityRepository;

    private final IdpStarterEmailService emailService;

    private final IdpStarterEmailProperties emailProperties;

    @Override
    public void restorePassword(String email) throws IdpStarterSendEmailError {
        val userEntity = userEntityRepository.findByEmail(email).orElseThrow(EntityNotFoundException::new);
        val restorePasswordEntity = IdpStarterUserRestorePasswordCodeEntity.builder()
                .userEntity(userEntity)
                .userId(userEntity.getId())
                .build();
        userEntity.setRestorePasswordCodeEntity(restorePasswordEntity);
        userEntityRepository.saveAndFlush(userEntity);

        emailService.sendMimeMessage(IdpStarterEmailSendDto.builder()
                .mailTo(userEntity.getEmail())
                .subject(emailProperties.getRestorePassword().getSubject())
                .text(emailProperties.getRestorePassword().getText().replaceAll(IdpStarterEmailProperties.LINK_REGEX,
                        emailProperties.getIdpServerAddress() + "/change-password?code="
                                + restorePasswordEntity.getCode())
                )
                .build());
    }
}
