package com.konigsberger.www.idp.starter.domain.email.service;

import com.konigsberger.www.idp.starter.domain.email.dto.IdpStarterEmailSendDto;
import com.konigsberger.www.idp.starter.domain.email.error.IdpStarterSendEmailError;

public interface IdpStarterEmailService {
    void sendSimpleMessage(final String to,final String subject,final String text) throws IdpStarterSendEmailError;

   void sendMimeMessage(IdpStarterEmailSendDto email) throws IdpStarterSendEmailError;
}
