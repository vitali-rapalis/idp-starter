package com.konigsberger.www.idp.starter.config;

import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@AutoConfiguration
@ComponentScan("com.konigsberger.www.idp.starter.*")
@EntityScan("com.konigsberger.www.idp.starter.*")
@EnableJpaRepositories("com.konigsberger.www.idp.starter.*")
@ConfigurationPropertiesScan("com.konigsberger.www.idp.starter.*")
@PropertySource("classpath:config/application-idp-starter.properties")
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
public class IdpStarterAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }

    @Configuration
    @Profile("dev")
    @PropertySource("classpath:config/dev/application-idp-starter-dev.properties")
    static class IdpStarterDevAutoConfiguration {

    }
}
