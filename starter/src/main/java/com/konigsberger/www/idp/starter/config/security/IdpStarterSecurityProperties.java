package com.konigsberger.www.idp.starter.config.security;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Data
@Builder
@Validated
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties("idp.security")
public class IdpStarterSecurityProperties {
    @NotBlank
    private String issuer;

    @NotBlank
    private String jwkEndpoint;

    @NotBlank
    private String logoutSuccessUrl;

    @NotNull
    private CorsProperties cors;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class CorsProperties {
        @Size(min = 1)
        private List<String> origins;

        @Size(min = 1)
        private List<String> methods;

        private List<String> headers;
    }
}
