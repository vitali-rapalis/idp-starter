package com.konigsberger.www.idp.starter.domain.user.error;

public class IdpStarterSignUpException extends Exception {
    private static final String MSG = "Sign up failed.";

    public IdpStarterSignUpException(Throwable ex) {
        super(MSG, ex);
    }
}
