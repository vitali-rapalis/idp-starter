package com.konigsberger.www.idp.starter.domain.consent;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorizationConsentEntityRepository extends JpaRepository<AuthorizationConsentEntity,
        AuthorizationConsentEntity.AuthorizationConsentId> {

    Optional<AuthorizationConsentEntity> findByRegisteredClientIdAndPrincipalName(String registeredClientId, String principalName);

    void deleteByRegisteredClientIdAndPrincipalName(String registeredClientId, String principalName);
}
