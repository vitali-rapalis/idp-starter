package com.konigsberger.www.idp.starter.domain.user.service;


import java.util.UUID;

public interface IdpStarterUserSignUpCodeService {

    UUID getUserId(UUID code);

    void deleteByCode(UUID code);
}
