package com.konigsberger.www.idp.starter.domain.user.error;

public class IdpStarterSignUpConfirmException extends Exception {

    private static final String ERROR_MSG = "Confirm sign up process failed.";

    public IdpStarterSignUpConfirmException(Throwable cause) {
        super(ERROR_MSG, cause);
    }
}
