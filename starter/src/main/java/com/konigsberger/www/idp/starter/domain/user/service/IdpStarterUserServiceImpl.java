package com.konigsberger.www.idp.starter.domain.user.service;

import com.konigsberger.www.idp.starter.domain.email.dto.IdpStarterEmailSendDto;
import com.konigsberger.www.idp.starter.domain.email.property.IdpStarterEmailProperties;
import com.konigsberger.www.idp.starter.domain.email.service.IdpStarterEmailService;
import com.konigsberger.www.idp.starter.domain.user.entity.*;
import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterChangePasswordException;
import com.konigsberger.www.idp.starter.domain.user.error.IdpStarterSignUpException;
import com.konigsberger.www.idp.starter.domain.user.model.IdpStarterUserDetailsModel;
import com.konigsberger.www.idp.starter.domain.user.repository.IdpStarterRestorePasswordCodeEntityRepository;
import com.konigsberger.www.idp.starter.domain.user.repository.IdpStarterUserEntityRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Log4j2
@Service
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class IdpStarterUserServiceImpl implements IdpStarterUserService {

    private final IdpStarterUserEntityRepository userRepository;

    private final IdpStarterRestorePasswordCodeEntityRepository passwordCodeEntityRepository;

    private final IdpStarterEmailService emailService;

    private final IdpStarterEmailProperties emailProperties;

    private final PasswordEncoder passwordEncoder;

    @Override
    public void signUp(String email, String password) throws IdpStarterSignUpException {
        try {
            userRepository.findByEmail(email).ifPresent(existedUser -> {
                throw new EntityExistsException();
            });

            val userEntity = IdpStarterUserEntity
                    .builder()
                    .email(email)
                    .password(passwordEncoder.encode(password))
                    .build();

            val userAccount = IdpStarterUserAccountEntity
                    .builder()
                    .userId(userEntity.getId())
                    .userEntity(userEntity)
                    .build();

            val userInfo = IdpStarterUserInfoEntity
                    .builder()
                    .userId(userEntity.getId())
                    .userEntity(userEntity)
                    .build();

            val signUpCodeEntity = IdpStarterUserSignUpCodeEntity.builder()
                    .userId(userEntity.getId())
                    .userEntity(userEntity)
                    .build();

            userEntity.setUserAccountEntity(userAccount);
            userEntity.setUserInfoEntity(userInfo);
            userEntity.setSignUpCodeEntity(signUpCodeEntity);
            userRepository.saveAndFlush(userEntity);

            emailService.sendMimeMessage(
                    IdpStarterEmailSendDto.builder()
                            .mailTo(email)
                            .subject(emailProperties.getSignUp().getSubject())
                            .text(emailProperties.getSignUp().getText()
                                    .replaceAll(IdpStarterEmailProperties.LINK_REGEX,
                                            emailProperties.getIdpServerAddress()
                                                    + "/sign-up-confirm?code=" + signUpCodeEntity.getCode())
                            )
                            .build()
            );
        } catch (Exception ex) {
            log.error("Sign up failed, reason: {}", ex.getMessage());
            throw new IdpStarterSignUpException(ex);
        }
    }

    @Override
    public IdpStarterUserDetailsModel changePassword(@NotNull final UUID code, @NotNull final String newPassword) throws IdpStarterChangePasswordException {
        try {
            val codeEntity = passwordCodeEntityRepository.findByCode(code).orElseThrow(EntityNotFoundException::new);
            val foundUser = userRepository.findById(codeEntity.getUserId()).orElseThrow(EntityNotFoundException::new);
            foundUser.setPassword(passwordEncoder.encode(newPassword));
            foundUser.setRestorePasswordCodeEntity(null);
            passwordCodeEntityRepository.deleteById(foundUser.getId());
            return new IdpStarterUserDetailsModel(foundUser);
        } catch (Exception ex) {
            log.error("Change password failed, reason: {}", ex.getMessage());
            throw new IdpStarterChangePasswordException(ex);
        }
    }

}

