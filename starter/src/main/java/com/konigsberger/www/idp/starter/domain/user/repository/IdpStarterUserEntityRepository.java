package com.konigsberger.www.idp.starter.domain.user.repository;

import com.konigsberger.www.idp.starter.domain.user.entity.IdpStarterUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface IdpStarterUserEntityRepository extends JpaRepository<IdpStarterUserEntity, UUID> {

    Optional<IdpStarterUserEntity> findByEmail(String email);

}
