package com.konigsberger.www.idp.starter.domain.user.error;

public class IdpStarterChangePasswordException extends Exception {

    public IdpStarterChangePasswordException(Throwable cause) {
        super("Change password failed.", cause);
    }
}
