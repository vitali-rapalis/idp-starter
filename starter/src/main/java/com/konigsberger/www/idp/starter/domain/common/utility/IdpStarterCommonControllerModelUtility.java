package com.konigsberger.www.idp.starter.domain.common.utility;

import com.konigsberger.www.idp.starter.domain.common.controller.IdpStarterCommonControllerModel;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserResponseMsgUtility;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.constraints.NotNull;
import lombok.experimental.UtilityClass;
import lombok.val;
import org.springframework.ui.Model;

import java.io.IOException;
import java.util.Optional;


@UtilityClass
public class IdpStarterCommonControllerModelUtility {

    private static final String COMMON_MODEL_NAME = "commonModel";

    public void createCommonModel(Optional<String> error, Model model, IdpStarterCommonControllerModel.Modal.EModalType type,
                                  String title, String msg) {
        if (error.isPresent()) {
            val commonModel = IdpStarterCommonControllerModel.builder()
                    .modal(IdpStarterCommonControllerModel.Modal.builder()
                            .type(type)
                            .title(title)
                            .text(msg)
                            .build())
                    .build();
            model.addAttribute(COMMON_MODEL_NAME, commonModel);
        }
    }

    public static void createCommonModelForSignIn(Model model, Optional<String> error, Optional<String> success) {
        val commonControllerModel = new IdpStarterCommonControllerModel();

        if (error.isPresent()) {
            var errorMsg = "";
            if (error.get().isBlank()) {
                errorMsg = IdpStarterUserResponseMsgUtility.SIGN_IN_FAILED;
            } else {
                errorMsg = IdpStarterCommonBase64Utility.decodeMessage(error.get());
            }
            commonControllerModel.setModal(
                    IdpStarterCommonControllerModel.Modal.builder()
                            .type(IdpStarterCommonControllerModel.Modal.EModalType.ERROR)
                            .title(IdpStarterCommonResponseMsgUtility.ERROR_TITLE)
                            .text(errorMsg)
                            .build());
        } else if (success.isPresent()) {
            var successMsg = "";
            if (success.get().isBlank()) {
                successMsg = IdpStarterUserResponseMsgUtility.SIGN_UP_CONFIRM_SUCCESS_MSG;
            } else {
                successMsg = IdpStarterCommonBase64Utility.decodeMessage(success.get());
            }
            commonControllerModel.setModal(
                    IdpStarterCommonControllerModel.Modal.builder()
                            .type(IdpStarterCommonControllerModel.Modal.EModalType.SUCCESS)
                            .title(IdpStarterCommonResponseMsgUtility.SUCCESS_TITLE)
                            .text(successMsg)
                            .build()
            );
        }

        model.addAttribute(COMMON_MODEL_NAME, commonControllerModel);
    }

    public static void redirectToError(@NotNull final HttpServletResponse response, @NotNull final String redirectUrl,
                                       @NotNull final String errorMsg) throws IOException {
        val encodedErrorMsg = IdpStarterCommonBase64Utility.encodeMessage(errorMsg);
        response.sendRedirect(redirectUrl + "?error=" + encodedErrorMsg);
    }
}
