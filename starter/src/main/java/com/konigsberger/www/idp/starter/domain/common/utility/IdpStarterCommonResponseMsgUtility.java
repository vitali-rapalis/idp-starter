package com.konigsberger.www.idp.starter.domain.common.utility;

import lombok.experimental.UtilityClass;

// TODO EXTERNALIZE MESSAGES TO A FILE
@UtilityClass
public class IdpStarterCommonResponseMsgUtility {

    public static final String ERROR_TITLE = "Error!";
    public static final String SUCCESS_TITLE = "Success!";
}
