package com.konigsberger.www.idp.starter.domain.user.utility;

import lombok.experimental.UtilityClass;

//  TODO MSG COULD BE EXTRACTED TO SEPARATED FILE
@UtilityClass
public class IdpStarterUserResponseMsgUtility {

    public static final String SIGN_UP_CONFIRM_FAILED_MSG = "Confirm email failed. Try again if it does not help, after " +
            "10 min you can try to register new.";

    public static final String SIGN_UP_ERROR_TEXT = "Sign up failed.";

    public static final String SIGN_UP_SUCCESS_TITLE = "Sign up Success.";

    public static final String SIGN_UP_SUCCESS_TEXT = "Successfully signed up, confirm your email address by clicking on the link we sent you in the email.";

    public static final String SIGN_IN_FAILED = "Sign in failed.";

    public static final String SIGN_UP_CONFIRM_SUCCESS_MSG = "Sign up confirmed successfully. You can now sign in.";

    public static final String RESTORE_PASSWORD_BAD_REQUEST = "Password and repeated password url parameter are required.";

    public static final String RESTORE_PASSWORD_ERROR_TEXT = "Restore password failed.";

    public static final String RESTORE_PASSWORD_SUCCESS_TITLE = "Restore password success.";

    public static final String RESTORE_PASSWORD_SUCCESS_TEXT = "Password was successfully restored. Check your email and submit new password.";

    public static final String CHANGE_PASSWORD_ERROR_TEXT = "Change password failed.";

    public static final String CHANGE_PASSWORD_BAD_REQUEST = "Password and repeated password fields must be set.";
}
