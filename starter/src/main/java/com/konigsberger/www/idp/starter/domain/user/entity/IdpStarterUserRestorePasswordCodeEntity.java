package com.konigsberger.www.idp.starter.domain.user.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import java.io.Serializable;
import java.util.UUID;

@Entity
@Table(name = "idp_user_restore_password_code")
@Data
@Builder
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
public class IdpStarterUserRestorePasswordCodeEntity implements Serializable {

    @Id
    @NotNull
    @Column(nullable = false, unique = true)
    @EqualsAndHashCode.Include
    private UUID userId;

    @NotNull
    @NaturalId
    @Column(nullable = false, unique = true)
    @Builder.Default
    private UUID code = UUID.randomUUID();

    @MapsId
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @ToString.Exclude
    private IdpStarterUserEntity userEntity;
}
