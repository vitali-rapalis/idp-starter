package com.konigsberger.www.idp.starter.config.security;

import com.konigsberger.www.idp.starter.domain.user.filter.IdpStarterChangePasswordFilter;
import com.konigsberger.www.idp.starter.domain.user.filter.IdpStarterUserSignUpConfirmFilter;
import com.konigsberger.www.idp.starter.domain.user.filter.IdpStarterUserSignUpFilter;
import com.konigsberger.www.idp.starter.domain.user.utility.IdpStarterUserUrlUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Log4j2
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class IdpStarterWebSecurityConfiguration {

    private final IdpStarterUserSignUpFilter signUpFilter;

    private final IdpStarterUserSignUpConfirmFilter signUpConfirmFilter;

    private final IdpStarterChangePasswordFilter changePasswordFilter;

    private final IdpStarterSecurityProperties starterProperties;

    private final OAuth2AuthorizedClientService authorizedClientService;

    @Bean
    public SecurityFilterChain webSecurity(HttpSecurity httpSecurity) throws Exception {
        IdpStarterFederatedIdentityConfigurer idpStarterFederatedIdentityConfigurer = new IdpStarterFederatedIdentityConfigurer()
                .oauth2UserHandler(new IdpStarterUserRepositoryOAuth2UserHandler());

        httpSecurity
                .csrf().disable() // TODO OPTIMIZE
                .cors(httpSecurityCorsConfigurer -> httpSecurityCorsConfigurer.configurationSource(corsConfigurationSource()))
                .formLogin(loginConfigurer -> loginConfigurer.permitAll()
                        .loginPage(IdpStarterUserUrlUtility.SIGN_IN)
                        .defaultSuccessUrl("/")
                        .usernameParameter("email")
                )
                .logout(logoutConfigurer -> logoutConfigurer.permitAll()
                        .logoutUrl(IdpStarterUserUrlUtility.SIGN_OUT)
                        .logoutSuccessUrl(starterProperties.getLogoutSuccessUrl())
                )
                .addFilterBefore(signUpFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(signUpConfirmFilter, UsernamePasswordAuthenticationFilter.class)
                .addFilterBefore(changePasswordFilter, UsernamePasswordAuthenticationFilter.class)
                .apply(idpStarterFederatedIdentityConfigurer)
                .and()
                .oauth2Login(oAuth2LoginConfigurer -> oAuth2LoginConfigurer.loginPage("/sign-in")
                        .permitAll().authorizedClientService(authorizedClientService))
                .authorizeHttpRequests()
                .requestMatchers("/login", "/logout", "/*.css", IdpStarterUserUrlUtility.SIGN_UP,
                        IdpStarterUserUrlUtility.SIGN_IN, "/registration", IdpStarterUserUrlUtility.SIGN_UP_SUCCESS,
                        IdpStarterUserUrlUtility.RESTORE_PASSWORD, IdpStarterUserUrlUtility.CHANGE_PASSWORD,
                        IdpStarterUserUrlUtility.RESTORE_PASSWORD_SUCCESS,
                        IdpStarterUserUrlUtility.SIGN_UP_CONFIRM, "/webjars/**", "/main.js", "/polyfills.js")
                .permitAll()
                .anyRequest().authenticated();
        return httpSecurity.build();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowedOrigins(starterProperties.getCors().getOrigins());
        config.setAllowedMethods(starterProperties.getCors().getMethods());
        config.setAllowedHeaders(starterProperties.getCors().getHeaders());
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", config);
        return source;
    }
}
