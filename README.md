# Identity Provider Spring Starter

---

> This project based on [Spring Authorization Server](https://github.com/spring-projects/spring-authorization-server).
> This spring starter adds extra functionality on top of spring authorization server project. Like sign-in and federated
> sign-in with custom login ui, sign-up, forgot-password, consensus and more.

## How to use

Add dependency to your pom.xml/build.gradle file. The dependency configures automatically.

### Gradle

```
build.gradle

repositories {
    maven {
        url 'https://gitlab.com/api/v4/projects/44118506/packages/maven'
    }
}

dependencies {
    implementation 'com.konigsberger.www.idp:starter:0.0.1'
}
```

### Maven

```
pom.xml

<repositories>
  <repository>
    <id>gitlab-konigsberger-idp</id>
    <url>https://gitlab.com/api/v4/projects/44118506/packages/maven</url>
  </repository>
</repositories>

<dependencies>
  <dependency>
    <groupId>com.konigsberger.www.idp</groupId>
    <artifactId>starter</artifactId>
    <version>0.0.1</version>
  </dependency>
</dependencies>
```