package com.konigsberger.www.idp.app;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

@Disabled
@DisplayName("The auto configuration if idp starter should be tested")
@SpringBootTest(
        classes = IdpApp.class,
        useMainMethod = SpringBootTest.UseMainMethod.ALWAYS,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
public class IdpStarterAutoConfigurationIntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @LocalServerPort
    private Integer port;

    @Test
    @Disabled
    @DisplayName("Dependencies for this test should not be null")
    void contextTest() {
        Assertions.assertThat(testRestTemplate).isNotNull();
        Assertions.assertThat(port).isNotNull();
    }

    @Test
    @DisplayName("Login page should be available, calling to /sign-in controller should respond with 200 Https status test")
    void loginPageShouldBeAvailableTest() {
        // When
        ResponseEntity<String> responseEntity = testRestTemplate.getForEntity
                (testRestTemplate.getRootUri() + "/sign-in", String.class);

        // Then
        Assertions.assertThat(responseEntity.getStatusCode().is2xxSuccessful()).isTrue();
    }
}