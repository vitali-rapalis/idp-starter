package com.konigsberger.www.idp.app.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("dev")
@RequiredArgsConstructor
public class IdpAppDevProfile {

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> {
        };
    }
}
