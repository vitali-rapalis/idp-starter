package com.konigsberger.www.idp.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IdpApp {

    public static void main(String[] args) {
        SpringApplication.run(IdpApp.class);
    }
}
