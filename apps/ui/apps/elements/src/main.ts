import {bootstrapApplication, createApplication} from "@angular/platform-browser";
import {
  CenterWrapperComponent, ChangePasswordComponent,
  RestorePasswordComponent,
  SignInComponent,
  SignUpComponent
} from "@idp-ui-client/shared/ui";
import {provideHttpClient} from "@angular/common/http";
import {createCustomElement} from "@angular/elements";
import {environment} from "./environments/environment";
import {AppComponent} from "./app/app.component";
import {PreloadAllModules, provideRouter, withPreloading} from "@angular/router";
import {APP_ROUTES} from "./app/app.routes";
import {provideAnimations} from "@angular/platform-browser/animations";

if (environment.production) {
  (async () => {
    const app = await createApplication({
      providers: [
        /* your global providers here */
        provideHttpClient()
      ],
    });

    const singInEl = createCustomElement(SignInComponent, {injector: app.injector});
    customElements.define('sh-ui-sign-in', singInEl);

    const signUpEl = createCustomElement(SignUpComponent, {injector: app.injector});
    customElements.define('sh-ui-sign-up', signUpEl);

    const restorePasswordEl = createCustomElement(RestorePasswordComponent, {injector: app.injector});
    customElements.define('sh-ui-restore-password', restorePasswordEl);

    const changePasswordEl = createCustomElement(ChangePasswordComponent, {injector: app.injector});
    customElements.define('sh-ui-change-password', changePasswordEl);

    const centerWrapperEl = createCustomElement(CenterWrapperComponent, {injector: app.injector});
    customElements.define('sh-ui-center-wrapper', centerWrapperEl);
  })();
} else {
  /*bootstrapApplication(AppComponent, {
    providers: [
      provideHttpClient(),
      provideRouter(
        APP_ROUTES,
        withPreloading(PreloadAllModules),
        // withDebugTracing(),
      ),
      provideAnimations()
    ]
  });*/
}
