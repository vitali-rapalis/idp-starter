import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {RouterLink, RouterOutlet} from "@angular/router";
import {CenterWrapperComponent} from "@idp-ui-client/shared/ui";

@Component({
  standalone: true,
  selector: 'idp-ui-client-root',
  template: `
    <ul class="row list-unstyled">
      <li class="col-auto">
        <a routerLink="/sign-in">Sign in</a>
      </li>
      <li class="col-auto">
        <a routerLink="/sign-up">Sign up</a>
      </li>
      <li class="col-auto">
        <a routerLink="/restore-password">Restore Password</a>
      </li>
      <li class="col-auto">
        <a routerLink="/change-password">Change Password</a>
      </li>
    </ul>
    <idp-ui-client-center-wrapper>
      <router-outlet></router-outlet>
    </idp-ui-client-center-wrapper>
  `,
  styles: [],
  imports: [
    RouterOutlet,
    CenterWrapperComponent,
    RouterLink
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppComponent {
}
