import {Routes} from "@angular/router";

export const APP_ROUTES: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: "full"
  },
  {
    path: 'sign-in',
    pathMatch: "full",
    loadComponent: () => import("@idp-ui-client/shared/ui").then(m => m.SignInComponent)
  },
  {
    path: 'sign-up',
    pathMatch: "full",
    loadComponent: () => import("@idp-ui-client/shared/ui").then(m => m.SignUpComponent)
  },
  {
    path: 'restore-password',
    pathMatch: "full",
    loadComponent: () => import("@idp-ui-client/shared/ui").then(m => m.RestorePasswordComponent)
  }, {
    path: 'change-password',
    pathMatch: "full",
    loadComponent: () => import("@idp-ui-client/shared/ui").then(m => m.ChangePasswordComponent)
  }
];
