import {Component, Input, OnInit} from '@angular/core';
import {AsyncPipe, JsonPipe, NgIf} from "@angular/common";
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";

@Component({
  standalone: true,
  selector: 'idp-ui-client-login',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss'],
  imports: [
    NgIf,
    AsyncPipe,
    JsonPipe,
    ReactiveFormsModule
  ],
})
export class SignInComponent implements OnInit {
  form: FormGroup | undefined;
  @Input() actionUrl = "/sign-in";
  @Input() signUpUrl = "/sign-up";
  @Input() title = "Sign In";
  @Input() signInBtnTxt = "Sign In";
  @Input() signUpBtnTxt = "Sign Up";
  @Input() restorePasswordUrl = "/restore-password";
  @Input() restorePasswordBtnTxt = "Restore Password";
  @Input() googleOauth2 = "/oauth2/authorization/google-idp";
  @Input() googleOauth2Enabled = true;
  @Input() googleOauth2Github = "/oauth2/authorization/github-idp";
  @Input() googleOauth2GithubEnabled = true;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    const passwordValidators = [Validators.required, Validators.minLength(6), Validators.maxLength(15)];
    this.form = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', passwordValidators),
    });
  }
}
