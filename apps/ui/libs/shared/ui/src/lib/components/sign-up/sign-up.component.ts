import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {NgIf} from "@angular/common";

@Component({
  standalone: true,
  selector: 'idp-ui-client-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  imports: [
    NgIf,
    ReactiveFormsModule
  ]
})
export class SignUpComponent implements OnInit{
  form: FormGroup | undefined;
  @Input() actionUrl = "/sign-up";
  @Input() signInUrl = "/sign-in";
  @Input() title = "Sign Up";
  @Input() signInBtnTxt = "Sign In";
  @Input() signUpBtnTxt = "Sign Up";
  @Input() restorePasswordUrl = "/restore-password";
  @Input() restorePasswordBtnTxt = "Restore Password";

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    const passwordValidators = [Validators.required, Validators.minLength(6), Validators.maxLength(15)];
    this.form = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
      password: this.fb.control('', passwordValidators),
      repeatedPassword: this.fb.control('', passwordValidators),
    });
  }
}
