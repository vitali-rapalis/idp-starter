import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {NgIf} from "@angular/common";
import {IdpUrls} from "../../utility/component.utility";
import {HttpClientModule} from "@angular/common/http";

@Component({
  standalone: true,
  selector: 'idp-ui-client-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  imports: [
    NgIf,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class ChangePasswordComponent implements OnInit {
  @Input() actionUrl = IdpUrls.CHANGE_PASSWORD;
  @Input() signUpUrl = IdpUrls.SIGN_UP;
  @Input() signUpBtnTxt = "Sign Up";
  @Input() signInUrl = IdpUrls.SIGN_IN;
  @Input() signInBtnTxt = "Sign In";
  @Input() title = "Change Password";
  @Input() changeBtnTxt = "Change";
  form: FormGroup | undefined;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    const url = new URL(window.location.href);
    const params = new URLSearchParams(url.search);
    const code = params.get('code');

    const passwordValidators = [Validators.required, Validators.minLength(6), Validators.maxLength(15)];
    this.form = this.fb.group({
      code: this.fb.control(code, Validators.required),
      password: this.fb.control('', passwordValidators),
      repeatedPassword: this.fb.control('', passwordValidators)
    });
  }
}
