import {Component} from '@angular/core';

@Component({
  standalone: true,
  selector: 'idp-ui-client-center-wrapper',
  templateUrl: './center-wrapper.component.html',
  styleUrls: ['./center-wrapper.component.scss'],
})
export class CenterWrapperComponent {
}
