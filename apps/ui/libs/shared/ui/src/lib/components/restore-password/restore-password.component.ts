import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {NgIf} from "@angular/common";

@Component({
  standalone: true,
  selector: 'idp-ui-client-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss'],
  imports: [
    NgIf,
    ReactiveFormsModule
  ]
})
export class RestorePasswordComponent implements OnInit {
  @Input() actionUrl = "/restore-password";
  @Input() signUpUrl = "/sign-up";
  @Input() signUpBtnTxt = "Sign Up";
  @Input() signInUrl = "/sign-in";
  @Input() signInBtnTxt = "Sign In";
  @Input() title = "Restore Password";
  @Input() restoreBtnTxt = "Restore";
  form: FormGroup | undefined;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: this.fb.control('', [Validators.required, Validators.email]),
    });
  }
}
