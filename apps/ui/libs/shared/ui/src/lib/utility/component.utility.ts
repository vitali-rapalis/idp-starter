export enum IdpUrls {
  SIGN_IN = "/sign-in",
  SIGN_UP = "/sign-up",
  SIGN_OUT = "/sign-out",
  RESTORE_PASSWORD = "/restore-password",
  CHANGE_PASSWORD = "/change-password",
}
